const getPromptNumber = (message = '') => {
    const userNumber = prompt(message);

    if(userNumber === null) {
        return;
    }

    if(!userNumber.length) {
        alert('Вы не ввели число. Попробуйте еще раз');
        return getPromptNumber(message);
    }

    return userNumber;
}

const getPromptOperator = (message = '') => {
    const operator = prompt(message);

    if(operator === null) {
        return;
    }

    if(!operator.length) {
        alert('Для дальнейшей работы с числами сначала введите оператор');
        return getPromptOperator(message);
    }

    return operator;
}

const calculatorCalcHandler = (operator, number1, number2) => {
    switch (operator) {
        case "+":
            return number1 + number2;
        case "/":
            return number1 / number2;
        case "*":
            return number1 * number2;
        case "-":
            return number1 - number2;
        default:
            return;
    }
}

const calculatorViewHandler = () => {
    const operator = getPromptOperator(`Введите оператор: 
      + (для сложения), 
      / (для деления), 
      * (для умножения), 
      - (для вычитания)
     `
    );

    if(operator === undefined) {
        return;
    }

    const firstNumber = getPromptNumber('Введите первое число');

    if(firstNumber === undefined) {
        return;
    }

    const secondNumber = getPromptNumber('Введите второе число');

    if(secondNumber === undefined) {
        return;
    }

    const result = calculatorCalcHandler(operator, +firstNumber, +secondNumber);
    alert(`Результат: ${result}`);
    console.log('debug result', result, typeof result);
};

calculatorViewHandler();
